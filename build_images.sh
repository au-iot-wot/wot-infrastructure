#!/bin/bash
pushd rabbitmq
  docker build . -t rokas/weather-rabbitmq:0.1
popd

pushd grafana
  docker build . -t rokas/weather-grafana:0.1
popd

